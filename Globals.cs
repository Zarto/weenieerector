﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using WeenieErector.Data;
using WeenieErector.Lib;
using WeenieErector.Views;

namespace WeenieErector
{
	public static class Globals
	{
		public static void Init(string pluginName, PluginHost host, CoreManager core)
		{
			PluginName = pluginName;

			Host = host;

			Core = core;

            WeenieSource = new WeenieSource();
        }

		public static string PluginName { get; private set; }

        public static PluginHost Host { get; private set; }

        public static CoreManager Core { get; private set; }

        public static SpawnManager SpawnManager { get; set; }

        public static MainView View { get; set; }

        public static MarkerManager MarkerManager { get; set; }

        public static LocationManager LocationManager { get; set; }

        public static WeenieSource WeenieSource { get; internal set; }
    }
}
