﻿namespace WeenieErector.Data.WorldSpawns {
    public class WeenieFrameOrigin {
        public double x = 0;
        public double y = 0;
        public double z = 0;
    }
}